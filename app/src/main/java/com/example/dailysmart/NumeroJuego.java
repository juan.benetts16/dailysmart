package com.example.dailysmart;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Random;

public class NumeroJuego extends AppCompatActivity {
    //Counters
    CountDownTimer countDownTimer3, countDownTimer5, countDownTimer7, countDownTimer9;

    //Components
    TextView tempoView;
    TextView cifraView;
    TextView aciertosView;
    Button startbtn, enterbtn;
    EditText cifraPuestaTxt;

    //Variables
    int result, inputed;
    int lvl = 1;
    int puntos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numero_juego);

        processUI();
        cifraPuestaTxt.setEnabled(false);
        enterbtn.setEnabled(false);
        tempoView.setVisibility(View.INVISIBLE);
        startbtn.setEnabled(true);

        countDownTimer3 = new CountDownTimer(4000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tempoView.setVisibility(View.VISIBLE);
                tempoView.setText(millisUntilFinished/1000 + "seg");
            }

            @Override
            public void onFinish() {
                cifraView.setText("??????");
                tempoView.setVisibility(View.INVISIBLE);
                enterbtn.setEnabled(true);
                cifraPuestaTxt.setEnabled(true);
                startbtn.setEnabled(false);
            }
        };

        countDownTimer5 = new CountDownTimer(6000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tempoView.setVisibility(View.VISIBLE);
                tempoView.setText(millisUntilFinished/1000 + "seg");
            }

            @Override
            public void onFinish() {
                cifraView.setText("??????");
                tempoView.setVisibility(View.INVISIBLE);
                enterbtn.setEnabled(true);
                cifraPuestaTxt.setEnabled(true);
                startbtn.setEnabled(false);
            }
        };

        countDownTimer7 = new CountDownTimer(8000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tempoView.setVisibility(View.VISIBLE);
                tempoView.setText(millisUntilFinished/1000 + "seg");
            }

            @Override
            public void onFinish() {
                cifraView.setText("??????");
                tempoView.setVisibility(View.INVISIBLE);
                enterbtn.setEnabled(true);
                cifraPuestaTxt.setEnabled(true);
                startbtn.setEnabled(false);
            }
        };

        countDownTimer9 = new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tempoView.setVisibility(View.VISIBLE);
                tempoView.setText(millisUntilFinished/1000 + "seg");
            }

            @Override
            public void onFinish() {
                cifraView.setText("??????");
                tempoView.setVisibility(View.INVISIBLE);
                enterbtn.setEnabled(true);
                cifraPuestaTxt.setEnabled(true);
                startbtn.setEnabled(false);
            }
        };

        startbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                randomNumber();
                if(lvl == 1)
                {
                    countDownTimer5.start();

                }
                else if(lvl == 2)
                {
                    countDownTimer5.start();
                }
                else if(lvl == 3)
                {
                    countDownTimer7.start();
                }
                else if(lvl == 4)
                {
                    countDownTimer9.start();
                }
            }
        });

        enterbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkNumber();
            }
        });
    }

    public void randomNumber()
    {
        result = ((int)(Math.random() * 30000)) + 1;
        cifraView.setText(String.valueOf(result));
    }

    public void checkNumber()
    {
        inputed = 0;
        try
        {
            inputed = Integer.parseInt(cifraPuestaTxt.getText().toString());
        }
        catch (NumberFormatException exp)
        {
            ///////////////////////////////////////////
        }
        if (result == inputed)
        {
            puntos = puntos + 10;
            Log.d("OK ME", "CORRECTO!" + result);
            aciertosView.setText("PUNTOS: " + puntos);
            result = 0;
            inputed = 0;
            startbtn.setEnabled(true);
            enterbtn.setEnabled(false);
            cifraPuestaTxt.setText("");
            cifraView.setText("CORRECTO" );
            cifraPuestaTxt.setEnabled(false);
        }
        else
        {
            puntos = puntos + 0;
            Log.d("OK ME", "INCORRECTO!" + result);
            aciertosView.setText("PUNTOS: " + puntos);
            startbtn.setEnabled(true);
            enterbtn.setEnabled(false);
            cifraPuestaTxt.setText("");
            cifraView.setText("INCORRECTO" );
            cifraPuestaTxt.setEnabled(false);
        }
    }

    public void processUI()
    {
        //Buttons
        startbtn = (Button) findViewById(R.id.comenzarbtn);
        enterbtn = (Button) findViewById(R.id.atinarbtn);
        //TextView Interactua
        tempoView = (TextView) findViewById(R.id.tempo); //EL TIEMPO
        cifraView = (TextView) findViewById(R.id.numeroAparecer); //APARECE EL NUMERO
        //EditText
        cifraPuestaTxt = (EditText) findViewById(R.id.respuesta); //LEE EL NUMERO
        //TextView Puntos
        aciertosView = (TextView) findViewById(R.id.aciertotxt); //LOS PUNTOS
    }


}
