package com.example.dailysmart;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MemoramaJuego extends AppCompatActivity {

    //Objetos de las imagenes
    ImageView iv_11, iv_12, iv_13, iv_21, iv_22, iv_23, iv_31, iv_32, iv_33, iv_41, iv_42, iv_43;

    //Array de las imagenes
    Integer[] cardsArray = {101, 102, 103, 104, 105, 106, 201, 202, 203, 204, 205, 206};

    //Imagenes actuales
    int image101, image102, image103, image104, image105, image106, image201, image202, image203, image204, image205, image206;

    int firstCard, secondCard;
    int clickFirst, clickSecond;
    int cardNumber = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memorama_juego);

        iv_11 = (ImageView)findViewById(R.id.a1);
        iv_12 = (ImageView)findViewById(R.id.a2);
        iv_13 = (ImageView)findViewById(R.id.a3);
        iv_21 = (ImageView)findViewById(R.id.b1);
        iv_22 = (ImageView)findViewById(R.id.b2);
        iv_23 = (ImageView)findViewById(R.id.b3);
        iv_31 = (ImageView)findViewById(R.id.c1);
        iv_32 = (ImageView)findViewById(R.id.c2);
        iv_33 = (ImageView)findViewById(R.id.c3);
        iv_41 = (ImageView)findViewById(R.id.d1);
        iv_42 = (ImageView)findViewById(R.id.d2);
        iv_43 = (ImageView)findViewById(R.id.d3);

        iv_11.setTag("0");
        iv_12.setTag("1");
        iv_13.setTag("2");
        iv_21.setTag("3");
        iv_22.setTag("4");
        iv_23.setTag("5");
        iv_31.setTag("6");
        iv_32.setTag("7");
        iv_33.setTag("8");
        iv_41.setTag("9");
        iv_42.setTag("10");
        iv_43.setTag("11");

        frontCardResources();
        Collections.shuffle(Arrays.asList(cardsArray));

        iv_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int theCard = Integer.parseInt((String)v.getTag());
                doStuff(iv_11, theCard);
            }
        });

        iv_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int theCard = Integer.parseInt((String)v.getTag());
                doStuff(iv_12, theCard);
            }
        });

        iv_13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int theCard = Integer.parseInt((String)v.getTag());
                doStuff(iv_13, theCard);
            }
        });

        iv_21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int theCard = Integer.parseInt((String)v.getTag());
                doStuff(iv_21, theCard);

            }
        });

        iv_22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int theCard = Integer.parseInt((String)v.getTag());
                doStuff(iv_22, theCard);

            }
        });

        iv_23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int theCard = Integer.parseInt((String)v.getTag());
                doStuff(iv_23, theCard);

            }
        });

        iv_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int theCard = Integer.parseInt((String)v.getTag());
                doStuff(iv_31, theCard);

            }
        });

        iv_32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int theCard = Integer.parseInt((String)v.getTag());
                doStuff(iv_32, theCard);

            }
        });

        iv_33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int theCard = Integer.parseInt((String)v.getTag());
                doStuff(iv_33, theCard);
            }
        });


        iv_41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int theCard = Integer.parseInt((String)v.getTag());
                doStuff(iv_41, theCard);
            }
        });

        iv_42.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int theCard = Integer.parseInt((String)v.getTag());
                doStuff(iv_42, theCard);
            }
        });

        iv_43.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int theCard = Integer.parseInt((String)v.getTag());
                doStuff(iv_43, theCard);
            }
        });
    }

    private void doStuff(ImageView iv, int card)
    {
        if(cardsArray[card] == 101)
        {
            iv.setImageResource(image101);

        }
        else if(cardsArray[card] == 102)
        {
            iv.setImageResource(image102);
        }
        else if(cardsArray[card] == 103)
        {
            iv.setImageResource(image103);
        }
        else if(cardsArray[card] == 104)
        {
            iv.setImageResource(image104);
        }
        else if(cardsArray[card] == 105)
        {
            iv.setImageResource(image105);
        }
        else if(cardsArray[card] == 106)
        {
            iv.setImageResource(image106);
        }
        else if(cardsArray[card] == 201)
        {
            iv.setImageResource(image201);
        }
        else if(cardsArray[card] == 202)
        {
            iv.setImageResource(image202);
        }
        else if(cardsArray[card] == 203)
        {
            iv.setImageResource(image203);
        }
        else if(cardsArray[card] == 204)
        {
            iv.setImageResource(image204);
        }
        else if(cardsArray[card] == 205)
        {
            iv.setImageResource(image205);
        }
        else if(cardsArray[card] == 206)
        {
            iv.setImageResource(image206);
        }

        //Check which image is selected and save it temporary
        if (cardNumber == 1)
        {
            firstCard = cardsArray[card];
            if (firstCard > 200)
            {
                firstCard = firstCard - 100;
            }
            cardNumber = 2;
            clickFirst = card;
            iv.setEnabled(false);
        }
        else if (cardNumber == 2)
        {
            secondCard = cardsArray[card];
            if (secondCard > 200)
            {
                 secondCard = secondCard - 100;
            }
            cardNumber = 1;
            clickSecond = card;

            iv_11.setEnabled(false);
            iv_12.setEnabled(false);
            iv_13.setEnabled(false);
            iv_21.setEnabled(false);
            iv_22.setEnabled(false);
            iv_23.setEnabled(false);
            iv_31.setEnabled(false);
            iv_32.setEnabled(false);
            iv_33.setEnabled(false);
            iv_41.setEnabled(false);
            iv_42.setEnabled(false);
            iv_43.setEnabled(false);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable () {
                @Override
                public void run() {
                    //Check if image is equal
                    calculate();
                }
            }, 500);
        }
    }

    private void calculate()
    {
        if(firstCard == secondCard)
        {
            //first click
            if (clickFirst == 0)
            {
                iv_11.setVisibility(View.INVISIBLE);
            }
            else if(clickFirst == 1)
            {
                iv_12.setVisibility(View.INVISIBLE);
            }
            else if(clickFirst == 2)
            {
                iv_13.setVisibility(View.INVISIBLE);
            }
            else if(clickFirst == 3)
            {
                iv_21.setVisibility(View.INVISIBLE);
            }
            else if(clickFirst == 4)
            {
                iv_22.setVisibility(View.INVISIBLE);
            }
            else if(clickFirst == 5)
            {
                iv_23.setVisibility(View.INVISIBLE);
            }
            else if(clickFirst == 6)
            {
                iv_31.setVisibility(View.INVISIBLE);
            }
            else if(clickFirst == 7)
            {
                iv_32.setVisibility(View.INVISIBLE);
            }
            else if(clickFirst == 8)
            {
                iv_33.setVisibility(View.INVISIBLE);
            }
            else if(clickFirst == 9)
            {
                iv_41.setVisibility(View.INVISIBLE);
            }
            else if(clickFirst == 10)
            {
                iv_42.setVisibility(View.INVISIBLE);
            }
            else if(clickFirst == 11)
            {
                iv_43.setVisibility(View.INVISIBLE);
            }

            //Second click
            if (clickSecond == 0)
            {
                iv_11.setVisibility(View.INVISIBLE);
            }
            else if(clickSecond == 1)
            {
                iv_12.setVisibility(View.INVISIBLE);
            }
            else if(clickSecond == 2)
            {
                iv_13.setVisibility(View.INVISIBLE);
            }
            else if(clickSecond == 3)
            {
                iv_21.setVisibility(View.INVISIBLE);
            }
            else if(clickSecond == 4)
            {
                iv_22.setVisibility(View.INVISIBLE);
            }
            else if(clickSecond == 5)
            {
                iv_23.setVisibility(View.INVISIBLE);
            }
            else if(clickSecond == 6)
            {
                iv_31.setVisibility(View.INVISIBLE);
            }
            else if(clickSecond == 7)
            {
                iv_32.setVisibility(View.INVISIBLE);
            }
            else if(clickSecond == 8)
            {
                iv_33.setVisibility(View.INVISIBLE);
            }
            else if(clickSecond == 9)
            {
                iv_41.setVisibility(View.INVISIBLE);
            }
            else if(clickSecond == 10)
            {
                iv_42.setVisibility(View.INVISIBLE);
            }
            else if(clickSecond == 11)
            {
                iv_43.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            iv_11.setImageResource(R.drawable.bgpink);
            iv_12.setImageResource(R.drawable.bgpink);
            iv_13.setImageResource(R.drawable.bgpink);
            iv_21.setImageResource(R.drawable.bgpink);
            iv_22.setImageResource(R.drawable.bgpink);
            iv_23.setImageResource(R.drawable.bgpink);
            iv_31.setImageResource(R.drawable.bgpink);
            iv_32.setImageResource(R.drawable.bgpink);
            iv_33.setImageResource(R.drawable.bgpink);
            iv_41.setImageResource(R.drawable.bgpink);
            iv_42.setImageResource(R.drawable.bgpink);
            iv_43.setImageResource(R.drawable.bgpink);
        }
        iv_11.setEnabled(true);
        iv_12.setEnabled(true);
        iv_13.setEnabled(true);
        iv_21.setEnabled(true);
        iv_22.setEnabled(true);
        iv_23.setEnabled(true);
        iv_31.setEnabled(true);
        iv_32.setEnabled(true);
        iv_33.setEnabled(true);
        iv_41.setEnabled(true);
        iv_42.setEnabled(true);
        iv_43.setEnabled(true);

        //Check if the game is over
        checkEnd();
    }

    private void checkEnd()
    {
        if(iv_11.getVisibility() == View.INVISIBLE &&
                iv_12.getVisibility() == View.INVISIBLE &&
                iv_13.getVisibility() == View.INVISIBLE &&
                iv_21.getVisibility() == View.INVISIBLE &&
                iv_22.getVisibility() == View.INVISIBLE &&
                iv_23.getVisibility() == View.INVISIBLE &&
                iv_31.getVisibility() == View.INVISIBLE &&
                iv_32.getVisibility() == View.INVISIBLE &&
                iv_33.getVisibility() == View.INVISIBLE &&
                iv_41.getVisibility() == View.INVISIBLE &&
                iv_42.getVisibility() == View.INVISIBLE &&
                iv_43.getVisibility() == View.INVISIBLE)
        {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MemoramaJuego.this);
            alertDialogBuilder
                    .setMessage("GAME OVER\n")
                    .setCancelable(false)
                    .setPositiveButton("NEW", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            Intent intent = new Intent(getApplicationContext(), MemoramaJuego.class);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setNegativeButton("EXIT", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    private void frontCardResources()
    {
        image101 = R.drawable.osu101;
        image102 = R.drawable.osu102;
        image103 = R.drawable.osu103;
        image104 = R.drawable.osu104;
        image105 = R.drawable.osu105;
        image106 = R.drawable.osu106;
        image201 = R.drawable.osu201;
        image202 = R.drawable.osu202;
        image203 = R.drawable.osu203;
        image204 = R.drawable.osu204;
        image205 = R.drawable.osu205;
        image206 = R.drawable.osu206;
    }
}
