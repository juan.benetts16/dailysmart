package com.example.dailysmart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class ReactionJuego extends AppCompatActivity {

    Button b_start, b_main;

    long startTime, endTime, currentTime, bestTime = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reaction_juego);

        b_start = (Button) findViewById(R.id.b_start); //Boton de start
        b_main = (Button) findViewById(R.id.b_main); //Boton grande

        TextView Scoretxt; //Declaramos el texto de score
        Scoretxt = (TextView) findViewById(R.id.Scoretxt); //Lo identificamos

        b_start.setEnabled(true); //Habilitamos el boton
        b_main.setEnabled(false); //Deshabilitamos el boton grande

        Scoretxt.setText("Mejor puntuaje - " + bestTime + "ms");


        int tiempo = ((int) Math.random() * 2000) + 1000; //Esto es para establecer el tiempo

        b_start.setOnClickListener(new View.OnClickListener() { //Declaramos el boton cuando sea presionado
            @Override
            public void onClick(View v)
            {

                b_start.setEnabled(false); //Deshabilitamos el strat
                b_main.setText("Espera..."); //Ponemos el texto de enmedio vacio

                Handler handler = new Handler(); //Utilizaremos un handler para hacer un Runnable
                handler.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    { //Metodo para el momento que corra el programa
                        startTime = System.currentTimeMillis(); //le decimos la cantidad de mili
                        b_main.setBackgroundColor( //Decimos el color que va estar la pantalla cuando presione algo
                                ContextCompat.getColor(getApplicationContext(), R.color.blue));

                        b_main.setText("PRESIONA");
                        b_main.setEnabled(true);
                    }
                    }, tiempo);
            }
        });

        b_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                endTime = System.currentTimeMillis();
                currentTime = endTime - startTime;
                b_main.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                b_main.setText(currentTime + "ms");
                b_start.setEnabled(true);
                b_main.setEnabled(false);

                if (currentTime < bestTime)
                {
                    Scoretxt.setText("Mejor puntuaje - " + currentTime + "ms");
                    bestTime = currentTime;
                }
            }
        });
    }
}
